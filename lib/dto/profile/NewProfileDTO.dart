import 'package:flutter/material.dart';

class NewProfileDTO {
  String name;
  String email;
  String password;
  String crm;

  NewProfileDTO({
    @required this.name,
    @required this.email,
    @required this.password,
    @required this.crm,
  });

  factory NewProfileDTO.fromJson(Map<String, dynamic> json) {
    return NewProfileDTO(
      name: json["name"],
      email: json["email"],
      password: json["password"],
      crm: json["crm"],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "name": name,
      "email": email,
      "password": password,
      "crm": crm,
    };
  }
}
