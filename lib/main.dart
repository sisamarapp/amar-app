import 'package:amarapp/utils/myStyles.dart';
import 'package:amarapp/views/calendar/calendar_screen.dart';
import 'package:flutter/material.dart';
import 'package:amarapp/router/router_generator.dart';
import 'package:flutter/services.dart';

void main() {
  //para impedir rotacionar tela
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: MyColors.green_1,
    // systemNavigationBarColor: Colors.blue,
  ));
  runApp(MaterialApp(
    title: "Contatos",
    debugShowCheckedModeBanner: false,
    // initialRoute: '/home',
    home: CalendarScreen(),
    onGenerateRoute: RouteGenerator.generateRoute,
    theme: ThemeData(
      primaryColor: MyColors.green,
      cursorColor: MyColors.blue,
      primaryColorBrightness: Brightness.dark,
    ),
  ));
}
