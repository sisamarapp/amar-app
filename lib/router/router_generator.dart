import 'package:flutter/material.dart';
import 'package:amarapp/views/signin/login_screen.dart';
import 'package:amarapp/views/home_screen.dart';
import 'package:amarapp/views/register/register_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    if (settings.name == '/login') {
      return MaterialPageRoute(builder: (_) => LoginScreen());
    } else if (settings.name == '/register') {
      return MaterialPageRoute(builder: (_) => RegisterScreen());
    } else if (settings.name == '/home') {
      return MaterialPageRoute(builder: (_) => HomeScreen());
    } else if (settings.name == '/') {
      print(settings.name);
      return null;
    } else {
      print(settings.name);
      return _errorRoute();
    }
    // switch (settings.name) {
    //   case '/login':
    //     return MaterialPageRoute(builder: (_) => LoginScreen());
    //   case '/register':
    //     return MaterialPageRoute(builder: (_) => RegisterScreen());
    //   case '/home':
    //     return MaterialPageRoute(builder: (_) => HomeScreen());
    //   // case '/contactPage':
    //   //   if (args != null) {
    //   //     return MaterialPageRoute(
    //   //       builder: (context) => ContactPage(
    //   //             contact: args,
    //   //           ),
    //   //     );
    //   //   }
    //   //   return _errorRouteObject();
    //   // case '/contactPage':
    //   //   return MaterialPageRoute(
    //   //     builder: (context) => ContactPage(
    //   //           contact: args,
    //   //         ),
    //   //   );
    //   default:
    //     return _errorRoute();
    // }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text(
                'ERROR',
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
              Text(
                "Mensagem desenvolvedor:",
                style: TextStyle(fontSize: 12.0),
              ),
              Text(
                "Erro na rota",
                style: TextStyle(fontSize: 12.0),
              ),
            ],
          ),
        ),
      );
    });
  }

  static Route<dynamic> _errorRouteObject() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text(
                'ERROR',
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
              Text(
                "Mensagem desenvolvedor:",
                style: TextStyle(fontSize: 12.0),
              ),
              Text(
                "objeto passado incorreto...",
                style: TextStyle(fontSize: 12.0),
              ),
            ],
          ),
        ),
      );
    });
  }
}
