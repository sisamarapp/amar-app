import 'dart:convert';

import 'package:http/http.dart' as http;

class HttpService {
  static final String _base = "http://10.70.80.203:8080/";
  final String _path;
  final Map<String, String> headers = {
    "Content-Type": "application/json",
    "accept": "application/json"
  };

  HttpService(this._path);

  Future<Map<String, dynamic>> post(Map<String, dynamic> body) {
    return http
        .post(_base + _path, headers: headers, body: json.encode(body))
        .then((http.Response response) {
      final int statusCode = response.statusCode;
    
      if (statusCode == 201) {
        return json.decode(response.body);
      }  if (statusCode < 200 || statusCode >= 400 || json == null) {
        throw Exception(json.decode(response.body));
      }
      throw Exception({"message": "Erro inesperado"});
    });
  }
}
