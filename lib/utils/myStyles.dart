import 'package:flutter/material.dart';

class MyColors {
  static final Color grey = Color.fromARGB(255, 86, 86, 86);
  static final Color greyLight = Color.fromARGB(255, 161, 162, 162);
  static final Color blue = Color.fromARGB(255, 123, 221, 229);
  static final Color blue_1 = Color.fromARGB(255, 160, 223, 250);
  static final Color green = Color.fromARGB(255, 113, 219, 212);
  static final Color green_1 = Color.fromARGB(255, 118, 204, 199);
  static final Color yellow = Color.fromARGB(255, 246, 194, 20);
  static final Color pink = Color.fromARGB(255, 235, 87, 119);
  static final Color white = Color.fromARGB(255, 255, 255, 255);
/**
  cor do botao: #F6C214
  cor do texto, do botao: #565757
  cor de fundo: #7BE5DD
 */
}

class MyTextStyles {
  static final TextStyle title =
      TextStyle(fontSize: 30.0, color: MyColors.grey);
  static final TextStyle title_1 =
      TextStyle(fontSize: 30.0, color: Colors.white);
  static final TextStyle title_2 =
      TextStyle(fontSize: 20.0, color: Colors.white);
  static final TextStyle title_3 =
      TextStyle(fontSize: 20.0, color: Colors.black);
  static final TextStyle title_4 =
      TextStyle(fontSize: 18.0, color: Colors.grey);
  static final TextStyle label =
      TextStyle(color: MyColors.greyLight, fontSize: 20.0);
  static final TextStyle textField =
      TextStyle(color: MyColors.grey, fontSize: 20.0);
  static final TextStyle textInformationUnderline = TextStyle(
      fontSize: 15.0,
      color: MyColors.grey,
      decoration: TextDecoration.underline);
  static final TextStyle textInformation =
      TextStyle(fontSize: 15.0, color: MyColors.grey);
  static final TextStyle textButtom = TextStyle(
      color: MyColors.grey, fontSize: 22.0, fontWeight: FontWeight.bold);

  static final TextStyle textButtom_1 = TextStyle(
      color: MyColors.grey, fontSize: 15.0, fontWeight: FontWeight.bold);
  static final TextStyle text = TextStyle(color: MyColors.grey, fontSize: 22.0);
  static final TextStyle text_1 = TextStyle(
      color: MyColors.grey, fontSize: 22.0, fontWeight: FontWeight.bold);
  static final TextStyle text_2 =
      TextStyle(color: Colors.white, fontSize: 18.0);
  static final TextStyle text_3 = TextStyle(
      color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.w700);
}
