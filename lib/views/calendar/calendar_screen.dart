import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:intl/intl.dart' show DateFormat;

class CalendarScreen extends StatefulWidget {
  @override
  _CalendarScreenState createState() => _CalendarScreenState();
}

class _CalendarScreenState extends State<CalendarScreen> {
  DateTime _currentDate = DateTime(2019, 2, 1);
  String _currentMonth = '';
  bool compactado = false;

  static Widget _eventIcon = new Container(
    decoration: new BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.blue, width: 2.0)),
    child: new Icon(
      Icons.notifications_none,
      color: Colors.amber,
    ),
  );

  EventList<Event> _markedDateMap = new EventList<Event>(
    events: {
      new DateTime(2019, 2, 10): [
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 1',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 2',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 3',
          icon: _eventIcon,
        ),
      ],
      new DateTime(2019, 3, 10): [
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 1',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 2',
          icon: _eventIcon,
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 3',
          icon: _eventIcon,
        ),
      ],
    },
  );
  CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;
  @override
  void initState() {
    /// Add more events to _markedDateMap EventList
    _markedDateMap.add(
        new DateTime(2019, 2, 25),
        new Event(
          date: new DateTime(2019, 2, 25),
          title: 'Event 5',
          icon: _eventIcon,
        ));

    _markedDateMap.add(
        new DateTime(2019, 2, 10),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 4',
          icon: _eventIcon,
        ));

    _markedDateMap.addAll(new DateTime(2019, 2, 11), [
      new Event(
        date: new DateTime(2019, 2, 11),
        title: 'Event 1',
        icon: _eventIcon,
      ),
      new Event(
        date: new DateTime(2019, 2, 11),
        title: 'Event 2',
        icon: _eventIcon,
      ),
      new Event(
        date: new DateTime(2019, 2, 11),
        title: 'Event 3',
        icon: _eventIcon,
      ),
      new Event(
        date: new DateTime(2019, 2, 11),
        title: 'Event 4',
        icon: _eventIcon,
      ),
      new Event(
        date: new DateTime(2019, 2, 11),
        title: 'Event 23',
        icon: _eventIcon,
      ),
      new Event(
        date: new DateTime(2019, 2, 11),
        title: 'Event 123',
        icon: _eventIcon,
      ),
    ]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _calendarCarouselNoHeader = CalendarCarousel<Event>(
      todayBorderColor: MyColors.yellow,
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate = date);
        events.forEach((event) => print(event.title));
      },
      weekendTextStyle: TextStyle(
        color: MyColors.pink,
        fontWeight: FontWeight.bold,
      ),
      thisMonthDayBorderColor: Colors.grey,
      weekFormat: compactado,
      markedDatesMap: _markedDateMap,
      height: compactado ? 80 : 350,
      selectedDateTime: _currentDate,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateShowIcon: true,
      markedDateIconMaxShown: 1,
      markedDateMoreShowTotal: false,
      showHeader: false,
      markedDateIconBuilder: (event) {
        return event.icon;
      },
      locale: "pt-br",
      showWeekDays: true,
      todayTextStyle: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
      ),
      todayButtonColor: Colors.green,
      selectedDayTextStyle: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
      ),
      selectedDayButtonColor: MyColors.yellow,
      maxSelectedDate: _currentDate.add(Duration(days: 60)),
      onCalendarChanged: (DateTime date) {
        this.setState(() => _currentMonth = DateFormat.yMMMM().format(date));
      },
      weekDayFormat: WeekdayFormat.short,
      weekDayMargin: EdgeInsets.all(0),
      weekdayTextStyle:
          TextStyle(fontWeight: FontWeight.bold, color: MyColors.pink),
      daysTextStyle: TextStyle(
          fontWeight: FontWeight.bold, color: Color.fromARGB(255, 65, 65, 65)),
      // inactiveWeekendTextStyle:
      //     TextStyle(fontWeight: FontWeight.bold, color: MyColors.pink),
      // inactiveDaysTextStyle: TextStyle(
      //     fontWeight: FontWeight.bold, color: Color.fromARGB(255, 65, 65, 65)),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Agenda de Eventos"),
      ),
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 0),
              child: new Row(
                children: <Widget>[
                  GestureDetector(
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: MyColors.pink,
                      size: 25,
                    ),
                    onTap: () {
                      setState(() {
                        _currentDate =
                            _currentDate.subtract(Duration(days: 30));
                        _currentMonth = DateFormat.yMMM().format(_currentDate);
                      });
                    },
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 15),
                      child: Text(
                        _currentMonth,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20.0,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    color: MyColors.yellow,
                    padding: EdgeInsets.symmetric(vertical: 6, horizontal: 15),
                    margin: EdgeInsets.only(right: 15),
                    child: GestureDetector(
                      child: Text(
                        compactado ? "Completo" : "Compacto",
                        style: MyTextStyles.textButtom_1,
                      ),
                      onTap: () {
                        setState(() {
                          compactado = !compactado;
                        });
                      },
                    ),
                  ),
                  GestureDetector(
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: MyColors.pink,
                        size: 25,
                      ),
                      onTap: () {
                        setState(() {
                          _currentDate = _currentDate.add(Duration(days: 30));
                          _currentMonth =
                              DateFormat.yMMM().format(_currentDate);
                        });
                      }),
                ],
              ),
            ),
            _calendarCarouselNoHeader,
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Notify(Colors.redAccent, "Evento 1", "texto aleatorio", ""),
                    Notify(
                        Colors.purpleAccent, "Evento 2", "texto aleatorio", ""),
                    Notify(
                        Colors.greenAccent, "Evento 3", "texto aleatorio", ""),
                    Notify(
                        Colors.blueAccent, "Evento 4", "texto aleatorio", ""),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Notify extends StatelessWidget {
  Color _color;
  String _title;
  String _text;
  String _img;
  Notify(this._color, this._title, this._text, this._img);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.all(5),
      height: 50,
      color: _color,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(_title),
          Text(_text),
        ],
      ),
    );
  }
}
