import 'package:amarapp/utils/myStyles.dart';
import 'package:amarapp/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _pageIndex = 0;

  static const List<Widget> _screens = <Widget>[
    Center(
        child: Icon(
      Icons.label,
      color: Colors.redAccent,
      size: 150.0,
    )),
    Center(
        child: Icon(
      Icons.dashboard,
      color: Colors.blueAccent,
      size: 150.0,
    )),
    Center(
        child: Icon(
      Icons.directions_run,
      color: Colors.deepPurpleAccent,
      size: 150.0,
    )),
  ];

  @override
  Widget build(BuildContext context) {
    List<BottomNavigationBarItem> _bottomNavBarItems =
        <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(
          Icons.label,
          size: 20.0,
          color:
              _pageIndex == 0 ? Theme.of(context).primaryColor : MyColors.grey,
        ),
        title: Text(
          "Label",
          style: TextStyle(
            fontSize: 20.0,
            color: _pageIndex == 0
                ? Theme.of(context).primaryColor
                : MyColors.grey,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.dashboard,
          size: 20.0,
          color:
              _pageIndex == 1 ? Theme.of(context).primaryColor : MyColors.grey,
        ),
        title: Text(
          "Dashboard",
          style: TextStyle(
            fontSize: 20.0,
            color: _pageIndex == 1
                ? Theme.of(context).primaryColor
                : MyColors.grey,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: Icon(
          Icons.directions_run,
          size: 20.0,
          color:
              _pageIndex == 2 ? Theme.of(context).primaryColor : MyColors.grey,
        ),
        title: Text(
          "Directions",
          style: TextStyle(
            fontSize: 20.0,
            color: _pageIndex == 2
                ? Theme.of(context).primaryColor
                : MyColors.grey,
          ),
        ),
      )
    ];

    Widget customNavBar = Material(
      elevation: 15.0,
      child: BottomNavigationBar(
        items: _bottomNavBarItems,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          setState(() {
            _pageIndex = index;
          });
        },
      ),
    );
    return Scaffold(
      appBar: AppBar(
        // iconTheme: IconThemeData(color: Colors.blue, size: 70.0),
        elevation: 0,
      ),
      drawer: CustomDrawer(),
      body: _screens[_pageIndex],
      bottomNavigationBar: customNavBar,
    );
  }
}
