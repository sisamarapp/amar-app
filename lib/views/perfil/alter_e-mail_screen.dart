import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';

class EmailScreen extends StatefulWidget {
  @override
  _EmailScreenState createState() => _EmailScreenState();
}

class _EmailScreenState extends State<EmailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("E-mail"),
      //   centerTitle: true,
      // ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 100.0),
            Row(
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Deseja alterar o seu ",
                          style:
                              TextStyle(color: Colors.black54, fontSize: 22.0),
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "e-mail?",
                              style: TextStyle(
                                  color: Colors.black, fontSize: 22.0),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 200,
            ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                  hintText: "carinaoliveira@gmail.com",
                ),
              ),
            ),
            SizedBox(
              height: 200,
            ),
            Center(
              child: Container(
                width: 500.0,
                height: 50,
                child: RaisedButton(
                  color: MyColors.yellow,
                  child: Text(
                    "Alterar",
                    style: TextStyle(color: MyColors.grey, fontSize: 25.0),
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
