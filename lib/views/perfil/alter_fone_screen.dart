import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';

class FoneScreen extends StatefulWidget {
  @override
  _FoneScreenState createState() => _FoneScreenState();
}

class _FoneScreenState extends State<FoneScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Telefone"),
      //   centerTitle: true,
      // ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 100.0),
            Row(
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Deseja alterar o seu número de ",
                          style:
                              TextStyle(color: Colors.black54, fontSize: 19.0),
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "telefone?",
                              style: TextStyle(
                                  color: Colors.black, fontSize: 19.0),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 200,
            ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                  hintText: "(88) 4002-8922",
                ),
              ),
            ),
            SizedBox(
              height: 200,
            ),
            Center(
              child: Container(
                width: 500.0,
                height: 50,
                child: RaisedButton(
                  color: MyColors.yellow,
                  child: Text(
                    "Alterar",
                    style: TextStyle(color: MyColors.grey, fontSize: 25.0),
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
