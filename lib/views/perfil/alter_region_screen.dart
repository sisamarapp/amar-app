import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';

class RegiaoScreen extends StatefulWidget {
  @override
  _RegiaoScreenState createState() => _RegiaoScreenState();
}

class _RegiaoScreenState extends State<RegiaoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(
      //     "Região de Atução",
      //   ),
      //   centerTitle: true,
      // ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 100.0),
            Row(
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Deseja alterar sua ",
                          style:
                              TextStyle(color: Colors.black54, fontSize: 19.0),
                        ),
                        Row(
                          children: <Widget>[
                            Text(
                              "região de atuação?",
                              style: TextStyle(
                                  color: Colors.black, fontSize: 19.0),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 160,
            ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                  hintText: "CE",
                  suffixIcon: Icon(Icons.arrow_downward),
                ),
              ),
            ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Aracati",
                  suffixIcon: Icon(Icons.arrow_downward),
                ),
              ),
            ),
            SizedBox(
              height: 200,
            ),
            Center(
              child: Container(
                width: 500.0,
                height: 50,
                child: RaisedButton(
                  color: MyColors.yellow,
                  child: Text(
                    "Alterar",
                    style: TextStyle(color: MyColors.grey, fontSize: 25.0),
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
