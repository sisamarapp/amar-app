import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';
import 'package:amarapp/views/perfil/alter_crm_screen.dart';
import 'package:amarapp/views/perfil/alter_e-mail_screen.dart';
import 'package:amarapp/views/perfil/alter_fone_screen.dart';
import 'package:amarapp/views/perfil/alter_name_screen.dart';
import 'package:amarapp/views/perfil/alter_password_screen.dart';
import 'package:amarapp/views/perfil/alter_region_screen.dart';

class PerfilScreen extends StatefulWidget {
  @override
  _PerfilScreenState createState() => _PerfilScreenState();
}

class _PerfilScreenState extends State<PerfilScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: Icon(
            Icons.chevron_left,
            size: 40.0,
          ),
          title: Text("PERFIL", style: MyTextStyles.title_2),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(35.0),
          child: Column(
            children: <Widget>[
              GestureDetector(
                child: Container(
                  child: Align(
                      alignment: Alignment.bottomRight,
                      child: CircleAvatar(
                        backgroundColor: MyColors.blue,
                        maxRadius: 15,
                        child: Icon(
                          Icons.photo_camera,
                          color: Colors.white,
                          size: 18.0,
                        ),
                      )),
                  width: 100.0,
                  height: 100.0,
                  decoration: BoxDecoration(
                    //color: Colors.red,
                    border: Border.all(color: MyColors.blue, width: 3.0),
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage("assets/images/user_avatar.png")),
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                child: GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Nome de preferência: ",
                          style: MyTextStyles.title_3,
                        ),
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(
                            "Carina Oliveira ",
                            style: MyTextStyles.title_4,
                          ),
                          Icon(
                            Icons.chevron_right,
                            color: Colors.grey,
                          ),
                        ],
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => NameScreen()));
                  },
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                child: GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "E-mail: ",
                          style: MyTextStyles.title_3,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            width: 200,
                            child: Text(
                              "carinaoliveira@gmail.com",
                              style: MyTextStyles.title_4,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Icon(
                            Icons.chevron_right,
                            color: Colors.grey,
                          )
                        ],
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => EmailScreen()));
                  },
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                child: GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "CRM:  ",
                          style: MyTextStyles.title_3,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "CE 0000",
                              style: MyTextStyles.title_4,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Icon(
                            Icons.chevron_right,
                            color: Colors.grey,
                          )
                        ],
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => CRMScreen()));
                  },
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                child: GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Região de atuação",
                          style: MyTextStyles.title_3,
                        ),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey,
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegiaoScreen()));
                  },
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
              Container(
                child: GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          "Número de telefone",
                          style: MyTextStyles.title_3,
                        ),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey,
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FoneScreen()));
                  },
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
              GestureDetector(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Alterar senha do aplicativo",
                            style: MyTextStyles.title_3,
                          ),
                        ),
                        Text(
                          "Utilizada para acessar o app",
                          style: MyTextStyles.title_4,
                        ),
                      ],
                    ),
                    Icon(
                      Icons.chevron_right,
                      color: Colors.grey,
                    )
                  ],
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PassWordScreen()));
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              Divider(
                color: Colors.grey,
              ),
              SizedBox(
                height: 10.0,
              ),
            ],
          ),
        ));
  }
}
