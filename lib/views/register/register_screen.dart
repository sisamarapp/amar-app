import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  Widget _name(BuildContext context) => Padding(
        padding: const EdgeInsets.only(
            top: 40.0, left: 15.0, right: 15.0, bottom: 20),
        child: Container(
          width: MediaQuery.of(context).size.width - 30,
          child: Card(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Form(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: MyColors.white),
                                child: GestureDetector(
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: MyColors.pink,
                                  ),
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 15.0),
                          RichText(
                            textAlign: TextAlign.justify,
                            text: TextSpan(
                              style: MyTextStyles.text,
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Digite seu ',
                                ),
                                TextSpan(
                                  text: 'nome completo',
                                  style: MyTextStyles.text_1,
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 0.0),
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              style: MyTextStyles.textField,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 50.0,
                      width: double.infinity,
                      child: RaisedButton(
                        color: MyColors.yellow,
                        onPressed: () {},
                        child: Text(
                          "Continuar",
                          style: MyTextStyles.textButtom,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.blue,
      body: _name(context),
    );
  }
}

class Profissional {
  int crm;
  int cpf;
  String nome;
  String profissao;
}
