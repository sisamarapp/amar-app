import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var controllerCPF = new MaskedTextController(mask: '000.000.000-00');

  Widget _BodyWidget(BuildContext context) => Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 80.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 120.0,
                  width: 100.0,
                  child: Image.asset(
                    "assets/images/logoapp20anos.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(right: 15.0, left: 15.0, top: 260.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width - 30,
                    child: Card(
                        child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Form(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.0),
                              child: Text(
                                "LOGIN",
                                style: MyTextStyles.title,
                              ),
                            ),
                            SizedBox(height: 15.0),
                            TextFormField(
                              controller: controllerCPF,
                              keyboardType: TextInputType.number,
                              textAlign: TextAlign.start,
                              decoration: InputDecoration(
                                labelText: "CPF",
                                labelStyle: MyTextStyles.label,
                                border: OutlineInputBorder(),
                              ),
                              style: MyTextStyles.textField,
                            ),
                            SizedBox(height: 10.0),
                            TextFormField(
                              obscureText: true,
                              textAlign: TextAlign.start,
                              decoration: InputDecoration(
                                labelText: "Senha",
                                labelStyle: MyTextStyles.label,
                                border: OutlineInputBorder(),
                              ),
                              style: MyTextStyles.textField,
                            ),
                            SizedBox(height: 10.0),
                            Column(
                              children: <Widget>[
                                Text("Esqueceu sua senha?",
                                    style:
                                        MyTextStyles.textInformationUnderline),
                                SizedBox(height: 5.0),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Não possui conta? ",
                                      style: MyTextStyles.textInformation,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushNamed('/register');
                                      },
                                      child: Text(
                                        "Cadastre-se",
                                        style: MyTextStyles
                                            .textInformationUnderline,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 50.0),
                            SizedBox(
                              height: 50.0,
                              width: double.infinity,
                              child: RaisedButton(
                                  onPressed: () {},
                                  child: Text(
                                    "Continuar",
                                    style: MyTextStyles.textButtom,
                                  ),
                                  color: MyColors.yellow),
                            ),
                          ],
                        ),
                      ),
                    )),
                  ),
                ),
              ],
            ),
          )
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      backgroundColor: MyColors.blue,
      body: _BodyWidget(context),
    );
  }
}
