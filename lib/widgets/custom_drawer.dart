import 'package:amarapp/utils/myStyles.dart';
import 'package:amarapp/widgets/tiles/drawer_tile.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class CustomDrawer extends StatefulWidget {
  @override
  _CustomDrawerState createState() => _CustomDrawerState();
}

class _CustomDrawerState extends State<CustomDrawer> {
  Map<String, dynamic> profissionalMap = {
    idColumn: 1,
    nameColumn: "Carina Oliveira",
    crmColumn: "CE 1231",
    emailColumn: "carinaoliveira@gmail.com",
    senhaColumn: "0123456789",
    ufColumn: "CE",
    cityColumn: "Fortaleza",
    phoneColumn: "(88)99012-3456",
    imgColumn: "assets/images/user_avatar.png"
  };
  Profissional profissional;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    profissional = Profissional.fromMap(profissionalMap);
  }

  Widget _drawerTopPart(Profissional profissional) => Container(
        padding: EdgeInsets.all(5.0),
        color: MyColors.green,
        height: 200.0,
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 90.0,
              width: 90.0,
              padding: EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: MyColors.blue_1, width: 5.5),
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("${profissional._img}")),
              ),
            ),
            Text(
              "Dra. ${profissional._name}",
              style: MyTextStyles.text_3,
            ),
            Text(
              "Clínica Médica",
              style: MyTextStyles.text_2,
            ),
            Text(
              "${profissional._city}, ${profissional._uf}",
              style: MyTextStyles.text_2,
            ),
          ],
        ),
      );

  Widget _drawerBottomPart(Profissional profissional) => Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 16.0, left: 32.0),
        child: Column(
          children: <Widget>[
            DrawerTile(Icons.person_outline, "Perfil", "asdasd"),
            DrawerTile(Icons.date_range, "Agenda de eventos", "asdasd"),
            DrawerTile(Icons.notifications_none, "Notificações", "asdasd"),
            DrawerTile(Icons.question_answer, "Chat online", "asdasd"),
            DrawerTile(Icons.chat_bubble_outline, "FAQ", "asdasd"),
            DrawerTile(Icons.info_outline, "Sobre este aplicativo", "asdasd"),
          ],
        ),
      );
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        child: ListView(
          children: <Widget>[
            _drawerTopPart(profissional),
            _drawerBottomPart(profissional),
          ],
        ),
      ),
    );
  }
}

final String contactTable = "ProfissionalTable";
final String idColumn = "idColumn";
final String nameColumn = "nameColumn";
final String crmColumn = "crmColumn";
final String emailColumn = "emailColumn";
final String senhaColumn = "senhaColumn";
final String ufColumn = "ufColumn";
final String cityColumn = "cityColumn";
final String phoneColumn = "phoneColumn";
final String imgColumn = "imgColumn";

class Profissional {
  int _id;
  String _name;
  String _crm;
  String _email;
  String _senha;
  String _uf;
  String _city;
  String _phone;
  String _img;
  Profissional();
  Profissional.fromMap(Map map) {
    _id = map[idColumn];
    _name = map[nameColumn];
    _crm = map[crmColumn];
    _email = map[emailColumn];
    _senha = map[senhaColumn];
    _uf = map[ufColumn];
    _city = map[cityColumn];
    _phone = map[phoneColumn];
    _img = map[imgColumn];
  }
  Map toMap() {
    Map<String, dynamic> mapa = {
      nameColumn: _name,
      crmColumn: _crm,
      emailColumn: _email,
      senhaColumn: _senha,
      ufColumn: _uf,
      cityColumn: _city,
      phoneColumn: _phone,
      imgColumn: _img
    };
    if (_id != null) mapa[idColumn] = _id;
    return mapa;
  }

  @override
  String toString() {
    return "Profisional(id: $_id, name: $_name, email: $_email, phone: $_phone, img: $_img)";
  }
}
