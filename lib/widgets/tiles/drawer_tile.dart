import 'package:amarapp/utils/myStyles.dart';
import 'package:flutter/material.dart';

class DrawerTile extends StatelessWidget {
  final IconData icon;
  final String text;
  final String page;
  DrawerTile(this.icon, this.text, this.page);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          height: 50.0,
          child: Row(
            children: <Widget>[
              Icon(
                icon,
                size: 32.0,
                color: MyColors.grey,
              ),
              SizedBox(width: 32.0),
              Text(text,
                  style: TextStyle(
                    fontSize: 18.0,
                    color: MyColors.grey,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
